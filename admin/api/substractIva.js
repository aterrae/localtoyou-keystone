module.exports = function (value, percentage) {
  return (value/(1 + percentage / 100)).toFixed(2);
}
