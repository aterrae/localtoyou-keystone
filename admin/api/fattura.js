var _ = require('underscore');
var async = require('async');
var keystone = require('../../');

var BollaOrdine = require('./controllers/bollaOrdine');
var BollaCassetta = require('./controllers/bollaCassetta');
var BollaAbbonamento = require('./controllers/bollaAbbonamento');

var FatturaOrdine = require('./controllers/fatturaOrdine');
var FatturaCassetta = require('./controllers/fatturaCassetta');
var FatturaAbbonamento = require('./controllers/fatturaAbbonamento');

var BollaIngressoMerci = require('./controllers/bollaIngressoMerci');
var BollaVenditaMerci = require('./controllers/bollaVenditaMerci');
var FatturaVenditaMerci = require('./controllers/fatturaVenditaMerci');

var BollaMercato = require('./controllers/bollaMercato');


exports = module.exports = function(req, res) {
  // Check pages
  if(req.params.list=='ordines' || req.params.list=='ordine-cassetta' || req.params.list=='abbonamentos' || req.params.list=='ingresso-mercis' || req.params.list=='vendita-mercis' || req.params.list=='mercatos') {
    // Check if objid exists
    if(req.params.objid!=null) {
      // Check if this fattura exists
      req.list.model.findById(req.params.objid)
      .populate('idUtente')
      .populate('idUtenteBusiness')
      .populate('cassette')
      .populate({ path: 'prodotti' })
      .exec(function(err, docs) {
        var prodOptions = {
          path: 'prodotti.produttore',
          model: 'Produttore'
        };
        var ivaOptions = {
          path: 'prodotti.iva',
          model: 'Iva'
        };

        if (err) return res.json(500);
          req.list.model.populate(docs, prodOptions, function (err, middle) {
            req.list.model.populate(middle, ivaOptions, function (err, result) {
            if(err==null){
              switch (req.params.list) {
                case 'ordines':
                FatturaOrdine(req, res, err, result);
                break;
                case 'ordine-cassetta':
                FatturaCassetta(req, res, err, result);
                break;
                case 'abbonamentos':
                FatturaAbbonamento(req, res, err, result);
                break;
                case 'ingresso-mercis':
                BollaIngressoMerci(req, res, err, result);
                break;
                case 'vendita-mercis':
                BollaVenditaMerci(req, res, err, result);
                break;
                case 'mercatos':
                BollaMercato(req, res, err, result);
                break;
                default:
                res.send("API not enabled for this collection");
                break;
              }
            } else {
              // This id doesn't exist
              res.send("No results found for id: " + req.params.objid);
            }
          });
        });
      });

    } else {
      // Download button from list view with filters
      var filters = req.list.processFilters(req.query.q);
      var queryFilters = req.list.getSearchFilters(req.query.search, filters);
      var keys = Object.keys(queryFilters);

      req.list.model.find(queryFilters)
      .populate('idUtente')
      .populate('idUtenteBusiness')
      .populate('cassette')
      .populate({ path: 'prodotti' })
      .exec(function(err, docs) {

        var prodOptions = {
          path: 'prodotti.produttore',
          model: 'Produttore'
        };

        if (err) return res.json(500);
        req.list.model.populate(docs, prodOptions, function (err, result) {
          if(err==null){
            switch (req.params.list) {
              case 'ordines':
              BollaOrdine(req, res, err, result);
              break;
              case 'ordine-cassetta':
              BollaCassetta(req, res, err, result);
              break;
              case 'abbonamentos':
              BollaAbbonamento(req, res, err, result);
              break;
              case 'vendita-mercis':
              FatturaVenditaMerci(req, res, err, result);
              break;
              default:
              res.send("API not enabled for this collection");
              break;
            }
          } else {
            // This id doesn't exist
            res.send("No results found");
          }
        });
      });
    }

  } else {
    // This page is not enabled
    res.send("API not enabled for this collection");
  }

};
