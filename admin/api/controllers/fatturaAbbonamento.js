var keystone = require('../../../');
var render = require('../render');
var path = require('path');
var pdf = require('html-pdf');
var pdfOptions = { format: 'A3' };
var DateConverter = require('../dateConverter');


module.exports = function (req, res, err, result) {
  if (err) {
    return res.apiError('error', err);
  }

  var templateContent = {};
  var totale = 0, costoSpedizione = 0;
  templateContent.cassette = [];

  if(result.cassette.length==0){
    res.send("Impossibile generare una fattura senza cassette");
  } else {

      templateContent.cassette[0] = {};
      templateContent.cassette[0].nome = result.cassette.nome;
      templateContent.cassette[0].produttore = "Local To You";
      templateContent.cassette[0].prezzo = result.cassette.prezzo.toFixed(2);
      templateContent.cassette[0].quantita = 1;
      templateContent.cassette[0].spedizione = result.dataSpedizione;

      templateContent.id = result.sku;
      templateContent.data = DateConverter(result.dataCreazione);
      templateContent.cliente = result.idUtente.nome + ' ' + result.idUtente.cognome;
      templateContent.indirizzoR1 = result.indirizzo.street1;
      templateContent.indirizzoR2 = result.indirizzo.state + ', ' + result.indirizzo.postcode + ' ' + result.indirizzo.country;
      templateContent.subtotale = result.cassette.prezzo.toFixed(2);
      if(totale < 45) costoSpedizione = 3.50;
      templateContent.spedizione = costoSpedizione.toFixed(2);
      templateContent.totale = (result.cassette.prezzo + costoSpedizione).toFixed(2);
      templateContent.logo = 'http://cdn.aterrae.com/localtoyou/logo.png';

      render(path.join(__dirname,'../documents/fatturaCassetta.hbs'), templateContent, function(err, html) {
        if (err) {
          return console.log(err);
        } else {
          console.log("PDF Generated");
        }

        // PDF Generator
        pdf.create(html, pdfOptions).toFile(__dirname+'/out/fatturaAbbonamentoCassetta.pdf', function(err, out) {
          if (err) return console.log(err);
          console.log(out);
          res.download(out.filename);
        });
      });
  }
}
