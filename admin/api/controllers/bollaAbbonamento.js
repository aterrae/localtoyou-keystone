var keystone = require('../../../');
var render = require('../render');
var path = require('path');
var pdf = require('html-pdf');
var pdfOptions = { format: 'A3' };


module.exports = function (req, res, err, result) {
  if (err) {
    return res.apiError('error', err);
  }

  var templateContent = {};
  templateContent.ordini = [];

  if(result.length==0){
    res.send("Impossibile generare una bolla senza abbonamenti");
  } else {

    var ordiniCount = 0;
    result.forEach(function(ordine){

      templateContent.ordini[ordiniCount]= {};
      templateContent.ordini[ordiniCount].sortingIndex = ordine.idSpedizione;
      templateContent.ordini[ordiniCount].cliente = ordine.idUtente.cognome + ' ' + ordine.idUtente.nome;
      templateContent.ordini[ordiniCount].indirizzoR1 = ordine.indirizzo.street1;
      templateContent.ordini[ordiniCount].indirizzoR2 = ordine.indirizzo.state + ', ' + ordine.indirizzo.postcode + ' ' + ordine.indirizzo.country;
      templateContent.ordini[ordiniCount].cassetta = {
        nome: ordine.cassette.nome,
        produttore: "Local To You",
        quantita: 1
      };

      templateContent.ordini.sort(function(a, b){
        if(a.sortingIndex < b.sortingIndex) return -1;
        if(a.sortingIndex > b.sortingIndex) return 1;
        if(a.cliente < b.cliente) return -1;
        if(a.cliente > b.cliente) return 1;
        return 0;
      });

      ordiniCount++;

    });

    templateContent.logo = 'http://cdn.aterrae.com/localtoyou/logo.png';

    render(path.join(__dirname,'../documents/bollaAbbonamento.hbs'), templateContent, function(err, html) {
      if (err) {
        return console.log(err);
      } else {
        console.log("PDF Generated");
      }

      // PDF Generator
      pdf.create(html, pdfOptions).toFile(__dirname+'/out/bollaAbbonamento.pdf', function(err, out) {
        if (err) return console.log(err);
        console.log(out);
        res.download(out.filename);
      });
    });
  }
}
