var keystone = require('../../../');
var render = require('../render');
var path = require('path');
var pdf = require('html-pdf');
var pdfOptions = { format: 'A3' };
var DateConverter = require('../dateConverter');


module.exports = function (req, res, err, result) {
  if (err) {
    return res.apiError('error', err);
  }

  var templateContent = {};
  var c = 0, totale = 0, costoSpedizione = 0;
  templateContent.cassette = [];

  if(result.cassette.length==0){
    res.send("Impossibile generare una fattura senza cassette");
  } else {
      result.cassette.forEach(function(cassetta){

        totale += cassetta.prezzo * result.quantita[c];
        templateContent.cassette[c] = {};
        templateContent.cassette[c].nome = cassetta.nome;
        templateContent.cassette[c].produttore = "Local To You";
        templateContent.cassette[c].prezzo = cassetta.prezzo.toFixed(2);
        templateContent.cassette[c].quantita = result.quantita[c];
        templateContent.cassette[c].spedizione = DateConverter(result.dataSpedizione);

        c++;

      });

      templateContent.id = result.sku;
      templateContent.data = DateConverter(result.dataCreazione);
      templateContent.cliente = result.idUtente.nome + ' ' + result.idUtente.cognome;
      templateContent.indirizzoR1 = result.indirizzo.street1;
      templateContent.indirizzoR2 = result.indirizzo.state + ', ' + result.indirizzo.postcode + ' ' + result.indirizzo.country;
      templateContent.subtotale = totale.toFixed(2);
      if(totale < 45) costoSpedizione = 3.50;
      templateContent.spedizione = costoSpedizione.toFixed(2);
      templateContent.totale = (totale + costoSpedizione).toFixed(2);
      templateContent.logo = 'http://cdn.aterrae.com/localtoyou/logo.png';

      render(path.join(__dirname,'../documents/fatturaCassetta.hbs'), templateContent, function(err, html) {
        if (err) {
          return console.log(err);
        } else {
          console.log("PDF Generated");
        }

        // PDF Generator
        pdf.create(html, pdfOptions).toFile(__dirname+'/out/fatturaCassetta.pdf', function(err, out) {
          if (err) return console.log(err);
          console.log(out);
          res.download(out.filename);
        });
      });
  }
}
