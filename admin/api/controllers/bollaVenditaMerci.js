var keystone = require('../../../');
var render = require('../render');
var path = require('path');
var pdf = require('html-pdf');
var pdfOptions = { format: 'A3' };
var DateConverter = require('../dateConverter');
var SubstractIva = require('../SubstractIva');


module.exports = function(req, res, err, result) {
  if (err) {
    return res.apiError('error', err);
  }

  var templateContent = {};
  var c = 0, totale = 0, totaleIvato = 0;
  templateContent.prodotti = [];

  if(result.prodotti.length==0){
    res.send("Impossibile generare una bolla senza prodotti");
  } else {

    result.prodotti.forEach(function(prodotto){

      totaleIvato += result.prezzo[c] * result.quantita[c];

      templateContent.prodotti[c] = {};
      templateContent.prodotti[c].nome = prodotto.nome;
      templateContent.prodotti[c].prezzoIva = result.prezzo[c].toFixed(2);
      templateContent.prodotti[c].prezzo = SubstractIva(result.prezzo[c], prodotto.iva.percentuale);
      templateContent.prodotti[c].quantita = result.quantita[c];
      templateContent.prodotti[c].unita = prodotto.quantita + prodotto.unitaMisura;
      templateContent.prodotti[c].produttore = prodotto.produttore.nome;

      totale += templateContent.prodotti[c].prezzo * result.quantita[c];

      c++;

    });

    templateContent.id = result.sku;
    templateContent.data = DateConverter(result.data);
    templateContent.totale = totale.toFixed(2);
    templateContent.totaleivato = totaleIvato.toFixed(2);
    templateContent.indirizzoR1 = result.idUtenteBusiness.indirizzo.street1;
    templateContent.indirizzoR2 = result.idUtenteBusiness.indirizzo.state + ', ' + result.idUtenteBusiness.indirizzo.postcode + ' ' + result.idUtenteBusiness.indirizzo.country;
    templateContent.piva = result.idUtenteBusiness.partitaIva;
    templateContent.cliente = result.idUtenteBusiness.ragioneSociale + ' | ' + result.idUtenteBusiness.nome + ' ' + result.idUtenteBusiness.cognome;
    templateContent.logo = 'http://cdn.aterrae.com/localtoyou/logo.png';

    render(path.join(__dirname,'../documents/bollaVenditaMerci.hbs'), templateContent, function(err, html) {
      if (err) {
        return console.log(err);
      } else {
        console.log("PDF Generated");
      }

      // PDF Generator
      pdf.create(html, pdfOptions).toFile(__dirname+'/out/bollaVenditaMerci.pdf', function(err, out) {
        if (err) return console.log(err);
        console.log(out);
        res.download(out.filename);
      });
    });
  }
}
