var keystone = require('../../../');
var render = require('../render');
var path = require('path');
var pdf = require('html-pdf');
var pdfOptions = { format: 'A3' };
var DateConverter = require('../dateConverter');


module.exports = function(req, res, err, result) {
  if (err) {
    return res.apiError('error', err);
  }

  var templateContent = {};
  templateContent.bolle = [];
  var currentDate = new Date();

  var c = 0,
      totaleBolla = 0,
      totale = 0;

  if(result.length==0){
    res.send("Impossibile generare una fattura senza bolle");
  } else {
    result.forEach(function(bolla){

      for (var i = 0; i < bolla.prodotti.length; i++) {
        totaleBolla += bolla.prezzo[i] * bolla.quantita[i];
      }

      templateContent.bolle[c] = {};
      templateContent.bolle[c].id = bolla.sku;
      templateContent.bolle[c].data =  DateConverter(bolla.data);
      templateContent.bolle[c].totale = totaleBolla.toFixed(2);
      totale += totaleBolla;

      c++;

    });

    templateContent.id = 1;
    templateContent.totale = totale.toFixed(2);
    templateContent.data = DateConverter(currentDate);
    templateContent.indirizzo = result[0].idUtenteBusiness.indirizzo.street1 + ' ' + result[0].idUtenteBusiness.indirizzo.postcode + ', ' + result[0].idUtenteBusiness.indirizzo.state + ' ' + result[0].idUtenteBusiness.indirizzo.country;
    templateContent.piva = result[0].idUtenteBusiness.partitaIva;
    templateContent.cliente = result[0].idUtenteBusiness.nome + ' ' + result[0].idUtenteBusiness.cognome;
    templateContent.logo = 'http://cdn.aterrae.com/localtoyou/logo.png';

    render(path.join(__dirname,'../documents/fatturaVenditaMerci.hbs'), templateContent, function(err, html) {
      if (err) {
        return console.log(err);
      } else {
        console.log("PDF Generated");
      }

      // PDF Generator
      pdf.create(html, pdfOptions).toFile(__dirname+'/out/fatturaVenditaMerci.pdf', function(err, out) {
        if (err) return console.log(err);
        console.log(out);
        res.download(out.filename);
      });
    });
  }
}
