var keystone = require('../../../');
var render = require('../render');
var path = require('path');
var pdf = require('html-pdf');
var pdfOptions = { format: 'A3' };
var DateConverter = require('../dateConverter');


module.exports = function (req, res, err, result) {
  if (err) {
    return res.apiError('error', err);
  }

  var templateContent = {};
  var c = 0, totale = 0, sconto = 0, costoSpedizione = 0;
  templateContent.prodotti = [];

  if(result.prodotti.length==0){
    res.send("Impossibile generare una fattura senza prodotti");
  } else {
    result.prodotti.forEach(function(prodotto){

      sconto += (prodotto.prezzo / 100) * prodotto.sconto * result.quantita[c];
      totale += prodotto.prezzo * result.quantita[c];
      templateContent.prodotti[c] = {};
      templateContent.prodotti[c].nome = prodotto.nome;
      templateContent.prodotti[c].produttore = prodotto.produttore.nome;
      templateContent.prodotti[c].prezzo = prodotto.prezzo.toFixed(2);
      templateContent.prodotti[c].sconto = prodotto.sconto;
      templateContent.prodotti[c].quantita = result.quantita[c];
      templateContent.prodotti[c].unita = prodotto.quantita + ' ' + prodotto.unitaMisura;

      c++;

    });

    templateContent.id = result.sku;
    templateContent.data = DateConverter(result.dataCreazione);
    templateContent.cliente = result.idUtente.nome + ' ' + result.idUtente.cognome;
    templateContent.indirizzoR1 = result.indirizzo.street1;
    templateContent.indirizzoR2 = result.indirizzo.state + ', ' + result.indirizzo.postcode + ' ' + result.indirizzo.country;
    templateContent.subtotale = totale.toFixed(2);
    templateContent.sconto = sconto.toFixed(2);
    if(totale < 45) costoSpedizione = 3.50;
    templateContent.spedizione = costoSpedizione.toFixed(2);
    templateContent.totale = (totale + costoSpedizione - sconto).toFixed(2);
    templateContent.logo = 'http://cdn.aterrae.com/localtoyou/logo.png';

    render(path.join(__dirname,'../documents/fatturaOrdine.hbs'), templateContent, function(err, html) {
      if (err) {
        return console.log(err);
      } else {
        console.log("PDF Generated");
      }

      // PDF Generator
      pdf.create(html, pdfOptions).toFile(__dirname+'/out/fatturaOrdine.pdf', function(err, out) {
        if (err) return console.log(err);
        console.log(out);
        res.download(out.filename);
      });
    });
  }
}
