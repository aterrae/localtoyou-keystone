var Handlebars = require('handlebars')
var fs = require('fs')

module.exports = function(tplPath, data, cb) {
  fs.readFile(tplPath, function(err, tpl) {
    if (err)
      cb(err)
    else
      var template = Handlebars.compile(tpl.toString())
      var rendered = template({result: data})
      cb(null, rendered)
  })
}
