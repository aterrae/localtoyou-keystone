module.exports = function (data) {

    var monthNames = ["Gennaio",
                      "Febbraio",
                      "Marzo",
                      "Aprile",
                      "Maggio",
                      "Giugno",
                      "Liglio",
                      "Agosto",
                      "Settembre",
                      "Ottobre",
                      "Novembre",
                      "Dicembre"];

    var dataFormatted = data.getDate() + ' ' + monthNames[data.getMonth()] + ' ' + data.getFullYear();

    return dataFormatted
}
